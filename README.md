# Stitch Movie Maker
Produce a movie from a list of image and video assets.

# Building for Compute Engine

    cd build
    mkdir out
    ./build-moviemaker out

This will produce a tarball in the `out` directory. You should upload
this to gs://stitch-opt/moviemaker/ and then reference it in the
startup-script.

There is a script to build ffmpeg as well. Follow the same instructions as above
but using the `build-ffmpeg` script.

# Running
systemd runs the service. A service file `/opt/etc/systemd/system/moviemaker.service`
defines how the service is run. The startup-script creates a symlink to this
file in `/etc/systemd/system`.

To start/stop the service use `systemctl`.

To view logs, use `sudo journalctl -f`
