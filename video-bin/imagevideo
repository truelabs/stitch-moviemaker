#!/bin/bash

usage() {
    echo "Usage: imagevideo <image_file> <duration> <output_file>"
    echo "Create a video file of length <duration> from an image file input."
    echo "See also:"
    echo "  https://trac.ffmpeg.org/wiki/Create%20a%20video%20slideshow%20from%20images"
}

INPUT_FILE=$1
shift
DURATION=$1
shift
OUTPUT_FILE=$1
shift

if [ -z "$INPUT_FILE" ]; then
    echo "Missing <image_file>"
    usage
    exit 1
fi

if [ -z "$DURATION" ]; then
    echo "Missing <duration>"
    usage
    exit 1
fi

if [ -z "$OUTPUT_FILE" ]; then
    echo "Missing <output_file>"
    usage
    exit 1
fi

# Convert image to a 30fps h.264 encoded mp4. The filter makes sure the dimentions are a multiple
# of 2, a requirement of the libx264 encoder.
ffmpeg -nostdin -y \
    -f image2 -framerate "1/$DURATION" -i "$INPUT_FILE" \
    -filter:v "pad=width=ceil(iw/2)*2:height=ceil(ih/2)*2:x=(ow-iw)/2:y=(oh-ih)/2:color=black" \
    -c:v libx264 -preset ultrafast -qp 0 \
    -r 30 \
    -pix_fmt +yuv420p \
    -f mp4 \
    "$OUTPUT_FILE"
