#!/bin/bash

usage() {
    echo "Usage: concat <input_list> <output>"
    echo "Concatenate input containers into a single output container."
    echo "Input files must use the same codec and codec parameters."
    echo "<input_list> is a text file that contains the path to video"
    echo "files, one per line."
    echo "See also:"
    echo "  https://trac.ffmpeg.org/wiki/Concatenate for more information"
    echo "  man ffmpeg-formats (look for the concat section)"
    echo "  ffmpeg --help demuxer=concat"
}

INPUTLIST=$1
shift
OUTPUT=$1
shift
if [ -z "$INPUTLIST" ]; then
    echo "Missing <input_list_file>"
    usage
    exit 1
fi

if [ -z "$OUTPUT" ]; then
    echo "Missing output"
    usage
    exit 1
fi

set -e
CONCATLIST=$(mktemp /tmp/concat-list.XXXXX)

function finish {
  echo "Cleaning up $CONCATLIST"
  rm "$CONCATLIST"
}
trap finish EXIT

echo "Building ffmpeg concat input file $CONCATLIST"
echo "ffconcat version 1.0" >> "$CONCATLIST"
while read -r line; do
    echo "file '$line'" >> "$CONCATLIST"
done < "$INPUTLIST"

ffmpeg -nostdin -y -f concat -safe 0 -i "$CONCATLIST" -c copy -f mp4 "$OUTPUT" 
