#!/bin/bash
# Build ffmpeg for moviemaker on a Debian instance.
# The build produces a tarball that when extracted has the
# following directory structure:
# opt/<package_name>

FFMPEG_VERSION=ffmpeg-2.7.2
OUTDIR=$1
shift

usage() {
    echo "build-ffmpeg <output_dir>"
    echo "Build ffmpeg for moviemaker on a Debian instance."
    echo "The tarball can be extracted on another debian"
    echo "instance with:"
    echo "  sudo tar --extract --file <tarball> --directory /"
}

if [ -z "$OUTDIR" ]; then
    echo "Missing output directory"
    usage
    exit 1
fi

OUTDIR=$(realpath "$OUTDIR")
if [ ! -d "$OUTDIR" ]; then
    echo "Output directory $OUTDIR is not a directory"
    usage
    exit 1
fi

set -e
WORKDIR=$(mktemp -d /tmp/ffmpeg-build-XXXXXX)
finish() {
    echo "Cleaning up $WORKDIR"
    rm -r "$WORKDIR"
}
trap finish EXIT

sudo apt-get update
sudo apt-get -y install xz-utils make gcc yasm fakeroot libfdk-aac-dev libx264-dev

# FFmpeg release signing key <ffmpeg-devel@ffmpeg.org>
# Got from pgp.mit.edu
gpg --import <<EOF
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: SKS 1.1.5
Comment: Hostname: pgp.mit.edu

mQENBE22rV0BCAC3DzRmA2XlhrqYv9HKoEvNHHf+PzosmCTHmYhWHDqvBxPkSvClipkbvJ4p
BnVvcX6mW5QyKhspHm5j1X5ibe9Bt9/chS/obnIobmvF8shSUgjQ0qRW9c1aWOjvT26SxYQ1
y9TmYCFwixeydGFHYKjAim+evGUccni5KMlfPoT3VTPtim78ufkr3E9Nco/Mobn/8APO0NmL
EGWAM6ln/8J/c9h6a1QKnQyBqWfT0YnAaebafFaZYwOtRdDG54VbJ4xwcHbCj5cKhTABk/Qt
BzDvnW4bG+uSpqdHbFZEY2JpURDuj/T3NudKQGzn0bYNpY1XY2l0pqs/btKHnBW0fVMjABEB
AAG0NEZGbXBlZyByZWxlYXNlIHNpZ25pbmcga2V5IDxmZm1wZWctZGV2ZWxAZmZtcGVnLm9y
Zz6JATgEEwECACIFAk22rV0CGwMGCwkIBwMCBhUIAgkKCwQWAgMBAh4BAheAAAoJELQyLwTW
dljYKxUH/1fqzl7SKie2g4t4PJbqUbkLuMsC+CP6gp0dcVZOHkuUYAoD3PM3iVxpLBVyKIXI
g7wMSTAtlIcYnzhWIpnoCBes6/O2Mrq6xHgGeTp6CDcm3LmmSYR1f5KdD8KUaA+lc/M/1fEn
wrSs/UGDk6R6iUmbqwxPsbozlOvmUHOLbDZBnKrk9XfAJdUhAuFACrSAT+KF1jniz0OfNGd2
3SaHWRCphoRW9pXDc5FfkdaueBUvBvGv19ZNcDhcxT3/u6z2DaUFC0rLWqk8obo951jVvi/z
OhB94Pw6u1SLvcTq3V1q5URWJtgSbpih9VRqxUbQNbXduKGzbHz6Vwpkupz4JReIRgQTEQIA
BgUCTgEHnAAKCRBhHseHBAsPq9gBAJ4uMZ8XQlo9cZICzqHjRZDwbTE/AgCfQzxSHCj37I7P
QdvAAtjHKd0oTlG5AQ0ETbatXQEIANjYrygJi/fn1nlSg5Mz0l9KHDm4yfWtaOrXUjJcyiGe
4G0XXJLGh45qxJ0DOKzi9id+9W4jby+kKuzG9O6Vn0iDeODOaOGnz4ua7Vu6d0AbYfNXZPWg
e/GCodo/ZD/qri1tPkLmRtT/sniahwy6LruPNHfFSRoNIjwbcD/IL+EbY1pL1/IFSzEAA1ZZ
amgmHgB7o9pwDIkK6HuvHMR/Y5MsoMfVfWV3ZGtA6v9z51CvnHsHPsADRSnUp7aYtR412SiA
O4XodMLTA92L3LxgYhI4ma7DXZ8jgKg4JkKO+DXmoU63HtRdq/HZjeXJKk1JGJF3zCvP3DyI
zZ8LWIjN8t0AEQEAAYkBHwQYAQIACQUCTbatXQIbDAAKCRC0Mi8E1nZY2LS8B/0bMoUAl4X9
D0WQbL4lU0czCIOKOsvbHpIxivjCnOQxU23+PV5WZdoCCpSuAHGv+2OHzhNrij++P9BNTJeQ
skxdS9FH4MZwy1IRSPrxegSxbCUpBI1rd0Zf7qb9BNPrHPTueWFV1uExOSB2ApsvWrKo2D8m
R0uZAPYfYl2ToFVoa5PR7/+ii9WiJr/flF6qm7hoLpI5Bm4VcZh2GPsJ9Vo/8x/qOGwtdWHq
BykYloKsrwD4U69rjn+d9feLoPBRgoVroXWQttt0sUnyoudz+x8ETJgPoNK3kQoDagApj4qA
t83Ayac3HzNIuEJ7LdvfINIOprujnJ9vH4n04XLgI4EZ
=rOF4
-----END PGP PUBLIC KEY BLOCK-----
EOF


cd "${WORKDIR}"
curl -O "http://ffmpeg.org/releases/${FFMPEG_VERSION}.tar.xz" 
curl -O "http://ffmpeg.org/releases/${FFMPEG_VERSION}.tar.xz.asc"
gpg --verify "${FFMPEG_VERSION}.tar.xz.asc" "${FFMPEG_VERSION}.tar.xz"
tar --extract --file "${FFMPEG_VERSION}.tar.xz"
cd "${FFMPEG_VERSION}"
./configure \
    --prefix="${WORKDIR}/opt" \
    --enable-nonfree \
    --enable-gpl \
    --disable-ffplay \
    --disable-ffserver \
    --disable-doc \
    --enable-libx264 \
    --enable-libfdk-aac
let NUM_JOBS=$(nproc)*2
make -j $NUM_JOBS
fakeroot make install
tar --directory "${WORKDIR}" \
    --create --xz --file "${OUTDIR}/${FFMPEG_VERSION}.tar.xz" \
    "./opt/bin" 

