package main

import (
	"bufio"
	"bytes"
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"github.com/drichardson/appengine/signedrequest"
	"github.com/drichardson/retry"
	"golang.org/x/net/context"
	"golang.org/x/oauth2/google"
	storage "google.golang.org/api/storage/v1"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"time"
)

type BucketObject struct {
	Bucket string `json:"bucket"`
	Object string `json:"object"`
}

type MovieMakerSpec struct {
	Assets       []BucketObject `json:"assets"`
	OutputBucket string         `json:"output_bucket"`

	// hex encoded content md5 is appended to this when writing the result.
	ObjectNamePrefix string `json:"object_name_prefix"`

	Callback signedrequest.SignedRequest `json:callback"`
}

func closeAndRemove(file *os.File) {
	if err := file.Close(); err != nil {
		log.Printf("Error file %v. Error: %v", file.Name(), err)
	}
	if err := os.Remove(file.Name()); err != nil {
		log.Printf("Error removing temporary file %v. Error: %v", file.Name(), err)
	}
}

var ErrInvalidArg = errors.New("Invalid Argument")
var ErrFailureResponse = errors.New("Failure Response")

func (m *MovieMakerSpec) process(c context.Context) error {
	log.Printf("Processing movie spec with callback url %v", m.Callback.URL)

	if m.OutputBucket == "" {
		log.Print("Empty bucket name.")
		return ErrInvalidArg
	}

	// Get the storage client before the other operations that take a long time so that we fail fast.
	client, err := google.DefaultClient(context.Background(), storage.DevstorageReadWriteScope)
	if err != nil {
		log.Print("Error getting default client for storage")
		return err
	}
	storageService, err := storage.New(client)
	if err != nil {
		log.Print("Error getting stoage service")
		return err
	}

	assets, err := ioutil.TempFile("", "moviemaker-assets")
	if err != nil {
		log.Print("Error getting moviemaker assets temp file")
		return err
	}
	defer closeAndRemove(assets)

	assetsWriter := bufio.NewWriter(assets)
	for _, asset := range m.Assets {
		gsUrl := "gs://" + asset.Bucket + "/" + asset.Object
		if _, err := assetsWriter.WriteString(gsUrl + "\n"); err != nil {
			log.Println("Error writing URL:", gsUrl)
			return err
		}
	}

	if err := assetsWriter.Flush(); err != nil {
		log.Print("assets writer flush failed")
		return err
	}

	outputMp4, err := ioutil.TempFile("", "moviemaker-output.mp4")
	if err != nil {
		log.Print("Error getting temporary file for movie maker output")
		return err
	}
	defer closeAndRemove(outputMp4)

	// The makemovie script is located in this source code repository under video/makemovie
	if output, err := exec.Command("makemovie", assets.Name(), outputMp4.Name()).CombinedOutput(); err != nil {
		log.Println("Error running moviemaker", err, "output:\n", string(output))
		return err
	} else {
		log.Print("makemovie output:\n", string(output))
	}

	if _, err := outputMp4.Seek(0, os.SEEK_SET); err != nil {
		log.Print("Error seeking to beginning of output mp4 to compute md5")
		return err
	}

	md5Bytes, err := ComputeMD5(outputMp4)
	if err != nil {
		log.Println("Error computing MD5 on", outputMp4.Name())
		return err
	}

	contentMd5 := base64.StdEncoding.EncodeToString(md5Bytes)
	md5Hex := hex.EncodeToString(md5Bytes)

	obj := &storage.Object{
		Bucket:       m.OutputBucket,
		ContentType:  "video/mp4",
		CacheControl: "public, max-age=31536000",
		Md5Hash:      contentMd5,
		Name:         m.ObjectNamePrefix + md5Hex,
	}

	if _, err := outputMp4.Seek(0, os.SEEK_SET); err != nil {
		log.Print("Error seeking to beginning of mp4 before upload")
		return err
	}

	log.Printf("Saving output to gs://%v/%v", m.OutputBucket, obj.Name)
	if _, err := storageService.Objects.Insert(m.OutputBucket, obj).Media(outputMp4).Do(); err != nil {
		log.Print("Error inserting object")
		return err
	}

	if m.Callback.URL != "" {
		callbackJSON := &BucketObject{
			Bucket: m.OutputBucket,
			Object: obj.Name,
		}
		callbackBody, err := json.Marshal(callbackJSON)
		if err != nil {
			log.Print("Error marshalling json response")
			return err
		}

		log.Printf("HTTP %v to callback URL %v", m.Callback.Method, m.Callback.URL)
		err = retry.BackoffRetryN(10, 10*time.Millisecond, 10*time.Second, func() error {
			req, err := m.Callback.HTTPRequest(bytes.NewReader(callbackBody))
			if err != nil {
				log.Printf("http.NewRequest failed. error=%v", err)
				return err
			}
			response, err := http.DefaultClient.Do(req)
			if err != nil {
				log.Printf("request failed. url=%v, error=%v", m.Callback.URL, err)
				return err
			}
			log.Printf("response returned StatusCode %v", response.StatusCode)
			if response.StatusCode >= 200 && response.StatusCode < 300 {
				log.Printf("callback response considered success")
				return nil
			} else {
				log.Printf("callback response considered error")
				return ErrFailureResponse
			}
		})

		if err != nil {
			return err
		}
	}

	return nil
}

func ComputeMD5(r io.Reader) ([]byte, error) {
	h := md5.New()
	if _, err := io.Copy(h, r); err != nil {
		log.Print("Error computing md5 digest")
		return nil, err
	}
	return h.Sum(nil), nil
}
