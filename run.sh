#!/bin/bash

if [ ! -d ./video ]; then
    echo "This script must be run from the moviemaker repository root directory."
    exit 1
fi

export GOPATH="$(pwd)"
export PATH="/dpac/ffmpeg-2.7.2/bin:$(realpath ./video-bin):$PATH"

set -e
go get moviemaker
go install moviemaker
bin/moviemaker
